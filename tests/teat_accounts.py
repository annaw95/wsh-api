import pytest
import requests
import endpoints
from faker import Faker

from models.account import Account

fake = Faker()


def test_get_accounts_list(new_account):
    response = requests.get(endpoints.account_list)
    assert response.status_code == 200  # jesli kod jest 200 to test pójdzie dalej
    print(response.json())
    response_dict = response.json()
    print(response_dict['accounts'])
    accounts_list = response_dict['accounts']
    names_list = [a['name'] for a in accounts_list]  # szuka w liscie śłowników wszystkich imion i podaje w słowniku
    print(names_list)
    assert new_account in names_list  # assert czyta to co jest po prawej i sprawdza czt taki string jest na liście names_list


def test_create_account(new_account):
    list_params = {'account': new_account.name}
    filter_list_response = requests.get(endpoints.account_list, params=list_params)
    assert filter_list_response.status_code == 200  # status ospowiedzi ma byc równy 200
    assert new_account.name in filter_list_response.text  # sprzawdzenie czy taki ciag znaków (rondam_name jest zawarty w takscie odpowiedzi


def test_delete_account(new_account):
    new_account.delete()
    delete_params = {'account': new_account.name}
    filter_list_response = requests.get(endpoints.account_list, params=delete_params)
    assert filter_list_response.status_code == 404  # sprawdzamy czy znalazł na liscie usunietego użytkownika - nie powinien wiec chcemy aby był kod 404


def test_account_balance(new_account):
    assert new_account.get_balance() == 1000

def test_last_test(new_account):
    new_account.pay(200)
    assert new_account.get_balance() == 1200
    new_account.withdraw(133)
    assert new_account.get_balance() == 1067


# funkcje oznaczamy adnotacja dekoratorem pytest i wtedy wszedzie gdzie podam accountPname pycharm wie, że chce użyć przed testem accound_name ibedzie przechowywana nazwa konta
@pytest.fixture
def new_account():
    account = Account()
    account.create()
    return account
