import requests
base_url = 'http://18.184.234.77:8080'

def test_ping():
    ping_response = requests.get(f'{base_url}/ping')
    assert ping_response.status_code == 200  # jesli kod jest 200 to test pójdzie dalej
    assert ping_response.json() == 'pong'


def test_ping_as_json():
    headers_dist = {
        'Accept': 'application/json'
    }

    ping_response = requests.get(f'{base_url}/ping', headers=headers_dist)
    assert ping_response.status_code == 200  # jesli kod jest 200 to test pójdzie dalej
    response_dict = ping_response.json()
    assert response_dict['reply'] == 'pong!'
